import { A11yService } from './a11y.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { PageTeaserComponent } from './page-teaser/page-teaser.component';
import { PageFooterComponent } from './page-footer/page-footer.component';
import { ModalDialogComponent } from './modal-dialog/modal-dialog.component';

const appRoutes: Routes = [
  { path: 'open-modal', component: ModalDialogComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PageHeaderComponent,
    PageTeaserComponent,
    PageFooterComponent,
    ModalDialogComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [
    A11yService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
