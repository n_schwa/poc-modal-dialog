import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { A11yService } from '../a11y.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.css']
})
export class ModalDialogComponent implements OnInit, OnDestroy {
  @ViewChild('close') public elClose: ElementRef;
  @ViewChild('save') public elSave: ElementRef;
  @ViewChild('modalDialog') public elModal: ElementRef;

  constructor(
    private router: Router,
    private a11y: A11yService
  ) { }

  ngOnInit() {
    this.elModal.nativeElement.focus();
    this.a11y.toggleModalShown(true);
  }

  ngOnDestroy() {
    this.a11y.toggleModalShown(false);
  }

  public closeModal(): void {
    this.router.navigate(['/']);
  }

  public closeWithEscape($event): void {
    console.log($event);
    if ($event.keyCode === 27) {
      this.closeModal();
    }
  }

  public focusLast(): void {
    this.elSave.nativeElement.focus();
  }

  public focusFirst(): void {
    this.elClose.nativeElement.focus();
  }
}
