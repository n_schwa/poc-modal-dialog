import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class A11yService {
  public broadcastModalShown: Subject<boolean> = new Subject();
  public isModalShown: boolean;

  constructor() {}

  public toggleModalShown(isShown: boolean) {
    this.isModalShown = isShown;
    this.broadcastModalShown.next(this.isModalShown);
  }


}
