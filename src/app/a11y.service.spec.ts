import { TestBed, inject } from '@angular/core/testing';

import { A11yService } from './a11y.service';

describe('A11yService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [A11yService]
    });
  });

  it('should be created', inject([A11yService], (service: A11yService) => {
    expect(service).toBeTruthy();
  }));
});
