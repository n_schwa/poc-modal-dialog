import { Component } from '@angular/core';
import { A11yService } from './a11y.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'app';

  public hideContent = false;

  constructor(private a11y: A11yService) {}

  ngOnInit() {
    this.initA11y();
  }

  public initA11y(): void {
    this.a11y.broadcastModalShown.subscribe( (isShown) => {
      this.hideContent = isShown;
      console.log('EVENT: modal showing?', isShown);
    });
  }
}
